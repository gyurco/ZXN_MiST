#!/bin/sh

echo -n > ZX_Next.qip
for vhd in $(find ZX_Next/ -name "*.vhd"); do
	echo "set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) $vhd      ]" >> ZX_Next.qip
done